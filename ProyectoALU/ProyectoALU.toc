\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Objetivos de aprendizaje}{3}{section.1}% 
\contentsline {section}{\numberline {2}Introducci\IeC {\'o}n}{3}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Software utilizado}{3}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}Licencia}{4}{subsection.2.2}% 
\contentsline {subsection}{\numberline {2.3}Videos YouTube}{4}{subsection.2.3}% 
\contentsline {section}{\numberline {3}Marco Te\IeC {\'o}rico}{4}{section.3}% 
\contentsline {subsection}{\numberline {3.1}L\IeC {\'o}gica Digital}{4}{subsection.3.1}% 
\contentsline {subsubsection}{\numberline {3.1.1}Compuerta NOT}{5}{subsubsection.3.1.1}% 
\contentsline {subsubsection}{\numberline {3.1.2}Compuerta AND y NAND}{5}{subsubsection.3.1.2}% 
\contentsline {subsubsection}{\numberline {3.1.3}Compuerta OR y NOR}{5}{subsubsection.3.1.3}% 
\contentsline {subsubsection}{\numberline {3.1.4}Compuerta XOR y XNOR}{6}{subsubsection.3.1.4}% 
\contentsline {subsubsection}{\numberline {3.1.5}Flip Flop J K}{7}{subsubsection.3.1.5}% 
\contentsline {subsection}{\numberline {3.2}Suma Binaria}{7}{subsection.3.2}% 
\contentsline {subsection}{\numberline {3.3}Resta Binaria}{8}{subsection.3.3}% 
\contentsline {subsubsection}{\numberline {3.3.1}Complemento a 1}{8}{subsubsection.3.3.1}% 
\contentsline {subsubsection}{\numberline {3.3.2}Complemento a 2}{9}{subsubsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.4}Multiplicaci\IeC {\'o}n}{10}{subsection.3.4}% 
\contentsline {subsection}{\numberline {3.5}Divisi\IeC {\'o}n}{10}{subsection.3.5}% 
\contentsline {section}{\numberline {4}Desarrollo}{11}{section.4}% 
\contentsline {section}{\numberline {5}Operaci\IeC {\'o}n ALU}{11}{section.5}% 
\contentsline {section}{\numberline {6}Sumador Restador}{12}{section.6}% 
\contentsline {subsection}{\numberline {6.1}Operaci\IeC {\'o}n}{13}{subsection.6.1}% 
\contentsline {subsection}{\numberline {6.2}Implementaci\IeC {\'o}n Complemento a 2}{13}{subsection.6.2}% 
\contentsline {subsection}{\numberline {6.3}Implementaci\IeC {\'o}n del sumador Restador}{13}{subsection.6.3}% 
\contentsline {section}{\numberline {7}Multiplicador}{15}{section.7}% 
\contentsline {subsection}{\numberline {7.1}Implementaci\IeC {\'o}n Multiplicaci\IeC {\'o}n}{16}{subsection.7.1}% 
\contentsline {section}{\numberline {8}Divisor}{17}{section.8}% 
\contentsline {subsection}{\numberline {8.1}Operaci\IeC {\'o}n}{17}{subsection.8.1}% 
\contentsline {subsection}{\numberline {8.2}Algoritmo divisi\IeC {\'o}n}{17}{subsection.8.2}% 
\contentsline {subsection}{\numberline {8.3}Generador de Ciclo de M\IeC {\'a}quina}{18}{subsection.8.3}% 
\contentsline {subsection}{\numberline {8.4}Memoria}{19}{subsection.8.4}% 
\contentsline {subsection}{\numberline {8.5}Comparador}{20}{subsection.8.5}% 
\contentsline {subsection}{\numberline {8.6}Contador}{22}{subsection.8.6}% 
\contentsline {subsection}{\numberline {8.7}Implementaci\IeC {\'o}n Final Divisi\IeC {\'o}n}{22}{subsection.8.7}% 
\contentsline {section}{\numberline {9}Seleccionador de Operaciones}{23}{section.9}% 
\contentsline {section}{\numberline {10}Integraci\IeC {\'o}n de Circuitos}{24}{section.10}% 
\contentsline {section}{\numberline {11}Discusi\IeC {\'o}n}{26}{section.11}% 
\contentsline {section}{\numberline {12}Conclusiones}{26}{section.12}% 
