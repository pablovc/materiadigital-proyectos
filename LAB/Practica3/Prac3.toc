\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Seguridad en la ejecuci\IeC {\'o}n}{1}% 
\contentsline {section}{\numberline {2}Objetivos de aprendizaje}{1}% 
\contentsline {section}{\numberline {3}Material y Equipo}{1}% 
\contentsline {section}{\numberline {4}Introducci\IeC {\'o}n}{2}% 
\contentsline {section}{\numberline {5}Actividad de Investigaci\IeC {\'o}n Previa}{2}% 
\contentsline {subsection}{\numberline {5.1}Inversor L\IeC {\'o}gico}{2}% 
\contentsline {section}{\numberline {6}Desarrollo}{2}% 
\contentsline {subsection}{\numberline {6.1}Planteamiento del problema}{2}% 
\contentsline {subsection}{\numberline {6.2}Actividad}{2}% 
\contentsline {subsection}{\numberline {6.3}Inversores en paralelo}{3}% 
\contentsline {subsection}{\numberline {6.4}Inversores en Serie}{4}% 
\contentsline {subsection}{\numberline {6.5}Actividad}{4}% 
\contentsline {section}{\numberline {7}Cuestionario}{5}% 
\contentsline {subsection}{\numberline {7.1}\IeC {\textquestiondown }Qu\IeC {\'e} es un circuito integrado com\IeC {\'u}n?}{5}% 
\contentsline {subsection}{\numberline {7.2}\IeC {\textquestiondown }Qu\IeC {\'e} es una funci\IeC {\'o}n de transferencia?}{5}% 
\contentsline {subsection}{\numberline {7.3}\IeC {\textquestiondown }Qu\IeC {\'e} es un inversor l\IeC {\'o}gico?}{6}% 
\contentsline {section}{\numberline {8}Conclusiones}{6}% 
